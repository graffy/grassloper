this directory contains a deployment solution based on docker to deliver grassloper to users

## how to build

```sh
graffy@graffy-ws2:~/work/grassloper$ docker build -t grassloper:1.0.0 ./grassloper.git
```

## how to use

```sh
graffy@graffy-ws2:~/work/grassloper$ docker run grassloper:1.0.0 -h
/usr/lib/python3.11/site-packages/h5py/__init__.py:36: UserWarning: h5py is running against HDF5 1.14.3 when it was built against 1.14.2, this may cause problems
  _warn(("h5py is running against HDF5 {0} when it was built against {1}, "
usage: grassloper [-h] --part-pos-file PART_POS_FILE --results-file
                  RESULTS_FILE [--part-pos-file-format {tractrac-hdf5}]
                  --particle-radius PARTICLE_RADIUS [--debug-dir DEBUG_DIR]

estimates the slope of granular surface

options:
  -h, --help            show this help message and exit
  --part-pos-file PART_POS_FILE
                        the input file containing the particle positions
  --results-file RESULTS_FILE
                        the output file containing for each frame one line
                        fitting the granular surface (hdf5 file format)
  --part-pos-file-format {tractrac-hdf5}
                        the file format of the input file that contains
                        particle positions
  --particle-radius PARTICLE_RADIUS
                        the radius of the particles in pixels (make sure that
                        the radius is big enough to unsure that neighbouring
                        particles touch each other)
  --debug-dir DEBUG_DIR
                        where to store image processing debug files

example: grassloper --part-pos-
file=./grassloper.git/samples/TracTrac/sample002_track.hdf5 --particle-
radius=11.0 --debug-dir='./improdebug' --results-file
./grassloper.git/samples/TracTrac/sample002_track_slopes.hdf5
```


