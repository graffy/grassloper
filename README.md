# grassloper
GRAnular Surface SLOPER: an application to estimate the slope of a granular surface flow

`grassloper` uses the output of [tractrac](https://perso.univ-rennes1.fr/joris.heyman/trac.html).

# how to install

`grassloper` is delivered as a simple pip application, and as such it can be installed from scratch using the following steps:

```sh
# create a python virtual environment (we name it grassloper.venv)
bob@stykades:~/work/grassloper$ python3 -m venv grassloper.venv  
# activate the virtual environment to use it
bob@stykades:~/work/grassloper$ source ./grassloper.venv/bin/activate
# grassloper requires improtools https://github.com/g-raffy/improtools, which is not in pip's catalog
# install improtools from its source directory (./improtools.git here, downloaded from https://github.com/g-raffy/improtools)
(grassloper.venv) bob@stykades:~/work/grassloper$ pip install -e ./improtools.git
# install grassloper from its source directory (./grassloper.git here)
# this automatically installs the packages that grassloper depends on
(grassloper.venv) bob@stykades:~/work/grassloper$ pip install -e ./grassloper.git
# now that grassloper is installed in grassloper.venv and grassloper.venv is activated, simply run it using its command line 'grassloper'
(grassloper.venv) bob@stykades:~/work/grassloper$ grassloper --help
```

# how to use

`grassloper`'s usage is described with the `--help` command line argument:

```sh
(grassloper.venv) bob@stykades:~/work/grassloper$ grassloper --help
```

`grassloper` uses the open source [hdf5 file format](https://en.wikipedia.org/wiki/Hierarchical_Data_Format#HDF5) to store its data. These files can be explored by [hdfview](https://www.hdfgroup.org/downloads/hdfview/) or [HDFCompass](https://support.hdfgroup.org/projects/compass/)
