from setuptools import setup

setup(
    name="grassloper",
    description="a tool to estimate the slope of a granular surface",
    version="1.1",
    author='Guillaume Raffy',
    author_email='guillaume.raffy@univ-rennes.fr',
    packages=['grassloper'],
    install_requires=['improtools(>=1.0)', 'numpy', 'opencv-python', 'scipy', 'h5py'],
    entry_points={
        'console_scripts': ['grassloper=grassloper.main:main', ]
    },
)
